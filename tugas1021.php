<?php
$nilai = 50;

if ($nilai >= 85) {
    echo "Nilai Anda $nilai, Anda mendapat nilai A (LULUS)";
} elseif ($nilai >= 70) {
    echo "Nilai Anda $nilai, Anda mendapat nilai B (LULUS)";
} elseif ($nilai >= 60) {
    echo "Nilai Anda $nilai, Anda mendapat nilai C (LULUS)";
} else {
    echo "Nilai Anda $nilai, Anda mendapat nilai E (GAGAL)";
}
?>
